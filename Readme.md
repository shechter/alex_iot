
# Simple IOT Simulator

1. Create a thing in the IoT Core console. 
2. Download the thing's X.509 certificates into the `certs` subdirectory. Update the `DEVICE_CERT` and `DEVICE_PRIVATE_KEY` environment variables in the `docker-compose.yaml` file  so that they point to the files you have downloaded.
3. Make note of the device's client ID and your AWS IoT custom endpoint. Update the `CLIENT_ID` and `IOT_ENDPOINT` environment values in `docker-compose.yaml`.
4. Create a Policy that will grant your devices access to the IoT service:
![Rule](https://public-mta.s3.amazonaws.com/iot_policy.png "IoT Policy granting your devices access to IoT actions")
5. Attach your policy to the already created certificate for your device:
![Rule](https://public-mta.s3.amazonaws.com/iot_policy_certificate.png "Attach your policy to your device certificate")
6. The device simulator will send data to the `devices/data` topic, so create an IoT Rule that selects messages from the `devices/data` topic
```
SELECT * FROM 'devices/data'
```
and sends it on to an Amazon Kinesis Stream of your choice:
![Rule](https://public-mta.s3.amazonaws.com/iot_rule_topic_to_kinesis.png "Rule to push data from topic to Kinesis")
7. Run the device simulator with `docker-compose up`



# Results

## Data received in the IoT Console

![data packet](https://public-mta.s3.amazonaws.com/Screen+Shot+2019-12-16+at+4.06.13+PM.png "Received JSON data packets")

## Data received in Kinesis

![data packet](https://public-mta.s3.amazonaws.com/data_in_kinesis.png "Received data in Kinesis")
