import os
import json
import time
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient

myMQTTClient = AWSIoTMQTTClient(os.getenv("CLIENT_ID"))
myMQTTClient.configureEndpoint(os.getenv("IOT_ENDPOINT"), 8883)


myMQTTClient.configureCredentials(os.getenv("AWS_CERT"), os.getenv("DEVICE_PRIVATE_KEY"), os.getenv("DEVICE_CERT"))

myMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

myMQTTClient.connect()
myMQTTClient.publish("devices/info", "connected", 0)
while 1:
	print(".")
	msg = { "id": os.getenv("CLIENT_ID"), "data": { "time": time.time()}}
	myMQTTClient.publish("devices/data", json.dumps(msg), 0)
	time.sleep(2)

#myMQTTClient.disconnect()
